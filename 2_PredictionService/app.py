from fastapi import FastAPI, HTTPException, Body

from transformers import BertConfig, BertTokenizerFast, TFBertModel
from tensorflow.keras.layers import Input, Dense, Dropout
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.initializers import TruncatedNormal
from pydantic import BaseModel
import tensorflow as tf
import uvicorn


import re
import emoji
import contractions
from nltk.corpus import stopwords
import nltk
from pymorphy2 import MorphAnalyzer
import numpy as np
import platform

app = FastAPI()

# загрузка предобученной модели BERT
model_name = 'bert-base-uncased'
config = BertConfig.from_pretrained(model_name, output_hidden_states=False)
tokenizer = BertTokenizerFast.from_pretrained(pretrained_model_name_or_path = model_name, config = config)
transformer_model = TFBertModel.from_pretrained(model_name, config = config)

max_length = 82

custom_stop_words = {'имя', 'ИМЯ'}

class TextRequest(BaseModel):
    text: str

# функция для создания BERT-модели
def create_model(nb_labels):

  # Load the MainLayer
  bert = transformer_model.layers[0]

  # Build the model inputs
  input_ids = Input(shape=(max_length,), name='input_ids', dtype='int32')
  attention_mask = Input(shape=(max_length,), name='attention_mask', dtype='int32')
  token_ids = Input(shape=(max_length,), name='token_type_ids', dtype='int32')  # Используйте 'token_type_ids' вместо 'token_ids'
  inputs = {'input_ids': input_ids, 'attention_mask': attention_mask, 'token_type_ids': token_ids}

  # Load the Transformers BERT model as a layer in a Keras model
  bert_model = bert(inputs)[1]
  dropout = Dropout(config.hidden_dropout_prob, name='pooled_output')
  pooled_output = dropout(bert_model, training=False)

  # Then build the model output
  emotion = Dense(units=nb_labels, activation="sigmoid", kernel_initializer=TruncatedNormal(stddev=config.initializer_range), name='emotion')(pooled_output)
  outputs = emotion

  # And combine it all in a model object
  model = Model(inputs=inputs, outputs=outputs, name='ruBERT_MultiLabel')

  return model




# Custom loss function for multilabel
def get_weighted_loss(weights):
    def weighted_loss(y_true, y_pred):
        return K.mean((weights[:,0]**(1-y_true))*(weights[:,1]**(y_true))*K.binary_crossentropy(y_true, y_pred), axis=-1)
    return weighted_loss

# создание инстанса модели
model = create_model(27)

# загрузка модели из каталога model/model
model_path = './Model/model/'
model.load_weights(model_path)

# компиляция модели
if platform.system() == "Darwin" and platform.processor() == "arm":
    optimizer = tf.keras.optimizers.legacy.Adam(learning_rate=3.e-05)
else:
    optimizer = tf.keras.optimizers.Adam(learning_rate=3.e-05)
model.compile(optimizer=optimizer, loss='binary_crossentropy')

# загрузка стоп-слов для русского языка
nltk.download('stopwords')
stop_words_russian = set(stopwords.words('russian'))

# инициализация морфологического словаря
morph_vocab = MorphAnalyzer()


# функция предобработки текста
def preprocess_text(x):
    # Добавление пробела между словами и знаками препинания
    x = re.sub(r'([а-яА-Я\[\]])([,;.!?])', r'\1 \2', x)
    x = re.sub(r'([,;.!?])([а-яА-Я\[\]])', r'\1 \2', x)

    # Нижний регистр
    x = x.lower()

    # Убираем смайлы
    x = emoji.demojize(x)

    # Расщирение сокращений
    x = contractions.fix(x)

    # Обработка смайлов
    x = re.sub(r"<3", "любовь", x)
    x = re.sub(r"xd", " улыбающееся лицо с открытым ртом и плотно закрытыми глазами ", x)
    x = re.sub(r":\)", " улыбающееся лицо ", x)
    x = re.sub(r"^_^", " улыбающееся лицо ", x)
    x = re.sub(r"\*_\*", "звезы в глазах", x)
    x = re.sub(r":\(", "хмурое лицо", x)
    x = re.sub(r":\^\(", "хмурое лицо", x)
    x = re.sub(r";\(", "хмурое лицо", x)
    x = re.sub(r":\/", "конфуз", x)
    x = re.sub(r";\)", " подмигивание", x)
    x = re.sub(r">__<", "неинтересно", x)
    x = re.sub(r"\b([xo]+x*)\b", "xoxo", x)
    x = re.sub(r"\b(n+a+h+)\b", "нет", x)

    # Обработка спецсимволов, пробелов итд
    x = re.sub(r"\b([.]{3,})", " dots ", x)
    x = re.sub(r"[^А-Яа-яA-Za-z!?_]+", " ", x)
    x = re.sub(r"\b([s])\b *", "", x)

    # Лемматизация
    x = ' '.join([morph_vocab.parse(word)[0].normal_form for word in x.split()])

    # Удаление стоп-слов
    x = ' '.join(
        [word for word in x.split() if word.lower() not in stop_words_russian and word not in custom_stop_words])

    # Удаление знаков препинания
    x = re.sub(r"[,.;!?]", "", x)

    x = re.sub(r" +", " ", x)
    x = x.strip()

    return x


def proba_to_labels(y_pred_proba, threshold=0.8):
    y_pred_labels = np.zeros_like(y_pred_proba)

    for i in range(y_pred_proba.shape[0]):
        for j in range(y_pred_proba.shape[1]):
            if y_pred_proba[i][j] > threshold:
                y_pred_labels[i][j] = 1
            else:
                y_pred_labels[i][j] = 0

    return y_pred_labels


taxonomy = ['sadness', 'love', 'gratitude', 'disapproval', 'amusement', 'disappointment', 'disgust', 'admiration',
            'realization', 'annoyance', 'confusion', 'optimism', 'curiosity', 'excitement', 'caring', 'joy', 'remorse',
            'approval', 'nervousness', 'embarrassment', 'surprise', 'anger', 'grief', 'pride', 'desire', 'relief',
            'fear']


# функция для предсказания эмоций
def predict_samples(text_samples):

    # Text preprocessing and cleaning
    text_samples_clean = [preprocess_text(text) for text in text_samples]

    # Tokenizing train data
    samples_token = tokenizer(
        text=text_samples_clean,
        add_special_tokens=True,
        max_length=82,
        truncation=True,
        padding='max_length',
        return_tensors='tf',
        return_token_type_ids=True,
        return_attention_mask=True,
        verbose=True,
    )

    # Preparing to feed the model
    samples = {
        'input_ids': samples_token['input_ids'],
        'attention_mask': samples_token['attention_mask'],
        'token_type_ids': samples_token['token_type_ids']
    }

    # Probability predictions
    samples_pred_proba = model.predict(samples)

    # Label prediction using threshold
    samples_pred_labels = proba_to_labels(samples_pred_proba, threshold)

    # Convert labels to emotion names
    samples_pred_emotions = [
        [taxonomy[i] for i in range(len(labels)) if labels[i] == 1] for labels in samples_pred_labels
    ]

    return samples_pred_emotions


threshold = 0.8




@app.post("/predict")
def check_depression(request: TextRequest):
    text = request.text
    prediction = predict_samples([str(text)])

    return {"text": text, "predict": prediction}


if __name__ == "__main__":
    uvicorn.run("app:app", host="127.0.0.1", port=5000, log_level="info", reload=True)
