import telebot
import requests
from datetime import datetime
from requests.exceptions import ReadTimeout

# Замените 'YOUR_BOT_TOKEN' на токен вашего бота
bot = telebot.TeleBot('6797863177:AAFwsIkwG4T2b_dHKvxhXplXM0AL5hezqwo')

server_url = 'http://127.0.0.1:5000/predict'
user_to_notify_id = '-4090761797'  # Замените на ID пользователя, которому отправлять уведомление
excluded_chat_id = -4090761797

# Определение критериев для разных типов
depressive_criteria = ['sadness', 'disappointment', 'grief']
suicidal_criteria = ['desire']
upset_criteria = ['anger', 'annoyance', 'disapproval']

def get_emotion_type(predicted_emotions):
    if any(emotion in depressive_criteria for emotion in predicted_emotions):
        return 'депрессивный'
    elif any(emotion in suicidal_criteria for emotion in predicted_emotions):
        return 'суицидный'
    elif any(emotion in upset_criteria for emotion in predicted_emotions):
        return 'расстроенный'

def is_chat_allowed(chat_id):
    return chat_id != excluded_chat_id

def handle_message(text, user_login, chat_id, chat_title, timestamp):
    max_retries = 3  # Максимальное количество попыток повторной отправки запроса
    retries = 0

    while retries < max_retries:
        try:
            # Отправляем сообщение на сервер для обработки
            response = requests.post(server_url, json={"text": text}, timeout=5)
            response.raise_for_status()  # Проверка на ошибку в HTTP-запросе
            processed_data = response.json()
            print(f'Processed data: {processed_data}')

            # Проверяем наличие "love" в массиве и отправляем уведомление в чат
            if "predict" in processed_data:
                predicted_emotions = processed_data["predict"][0]
                emotion_type = get_emotion_type(predicted_emotions)
                print(emotion_type)
                if emotion_type:
                    notification_text = f'Пользователь @{user_login} в чате "{chat_title}" отправил фразу "{text}" с эмоциями типа "{emotion_type}", пожалуйста, обратите внимание.'

                    # Отправляем уведомление в чат бота
                    bot.send_message(user_to_notify_id, notification_text)
                    print('Notification sent successfully')

            # Если мы дошли до этого момента, запрос был успешно обработан, выходим из цикла и обнуляем счетчик попыток
            retries = 0
            break
        except ReadTimeout:
            retries += 1
            print(f'Read timeout occurred. Retrying ({retries}/{max_retries})...')
            # Здесь вы можете добавить логику для повторной отправки запроса
        except Exception as e:
            print(f'Error processing message: {e}')
            break

@bot.message_handler(func=lambda message: is_chat_allowed(message.chat.id))
def handle_all_messages(message):
    text = message.text
    user_login = message.from_user.username
    chat_id = message.chat.id if message.chat else None
    chat_title = message.chat.title if message.chat and message.chat.title else None
    timestamp = message.date  # временная метка сообщения

    print(
        f'Received message {text} from @{user_login} in chat "{chat_title}" (ID: {chat_id}) at {datetime.fromtimestamp(timestamp)}:')

    handle_message(text, user_login, chat_id, chat_title, timestamp)

if __name__ == '__main__':
    bot.polling(none_stop=True)
